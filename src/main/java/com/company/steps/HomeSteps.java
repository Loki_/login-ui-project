package com.company.steps;

import com.company.pages.HomePage;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Home Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Home' page<br>
 */
@Slf4j
public class HomeSteps {

    private HomePage homePage;

    public HomeSteps(HomePage homePage){
        this.homePage = homePage;
    }

    public void verifyHomePageIsDisplayed() {
        assertTrue(homePage.isDisplayedHomePage(), "'Home' page is not displayed");
    }

    public void verifyHomeUrlIsCorrect() {
        assertTrue(homePage.isCorrectHomePageUrl(), "'Home' url is not correct");
    }

}
