package com.company.steps;

import com.company.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by anna on 05.10.17.
 */
public class PreconditionSteps extends FirstStep{
    private LoginPage loginPage = new LoginPage(getDriver());

    public PreconditionSteps(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);

    }

    public LoginSteps openLoginPage(){
        loginPage.openLoginPage();
        return new LoginSteps(loginPage);
    }


}
