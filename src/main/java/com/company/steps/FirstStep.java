package com.company.steps;

import org.openqa.selenium.WebDriver;

/**
 * Created by anna on 05.10.17.
 */
public class FirstStep {
    private WebDriver driver;

    public FirstStep(WebDriver driver){
        this.driver = driver;
    }

    protected WebDriver getDriver() {
        return driver;
    }

}
