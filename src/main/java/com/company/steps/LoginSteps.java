package com.company.steps;

import com.company.pages.HomePage;
import com.company.pages.LoginPage;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Login Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Login' page<br>
 */
@Slf4j
public class LoginSteps {
    private LoginPage loginPage;

    public LoginSteps(LoginPage loginPage){
        this.loginPage = loginPage;
    }

    public void verifyLoginPageIsDisplayed() {
        assertTrue(loginPage.isDisplayedLoginPage(), "'Login' page is not displayed");
    }

    public void verifyLoginUrlIsCorrect() {
        assertTrue(loginPage.isCorrectLoginPageUrl(), "'Login' url is not correct");
    }

    public HomeSteps makeLoginViaUser(String login, String pass){
        loginPage.isDisplayedLoginField();
        loginPage.typeUserLogin(login);
        loginPage.isDisplayedPasswordField();
        loginPage.typeUserPassword(pass);
        loginPage.isEnabledSubmitButton();
        HomePage homePage = loginPage.clickSubmitButton();
        homePage.waitElement();
        return new HomeSteps(homePage);
    }

    public void verifyIncorrectAlertIsDisplayed(){
        assertTrue(loginPage.isDisplayedIncorrectAlert(), "'Incorrect alert' is not displayed");
    }

}
