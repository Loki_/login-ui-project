package com.company.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Home page
 */
public class HomePage extends BasePage{

    private static final String HOME_PAGE_URL = "https://my-sandbox.maxpay.com/app.php#/app/dashboard";

    private static final String HOME_NAVIGATION_BAR_XPATH = "//nav[@class='ng-scope']";


    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = HOME_NAVIGATION_BAR_XPATH)
    private WebElement homeNavigationBar;

    public boolean isDisplayedHomePage(){
        return homeNavigationBar.isDisplayed();
    }

    public boolean isCorrectHomePageUrl(){
        return getDriver().getCurrentUrl().equals(HOME_PAGE_URL);
    }

}
