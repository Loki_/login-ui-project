package com.company.pages;

import com.company.steps.HomeSteps;
import com.sun.xml.internal.ws.api.server.WebServiceContextDelegate;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Login page
 */

public class LoginPage extends BasePage{

    private static final String LOGIN_URL = "https://my-sandbox.maxpay.com/#/signin";

    private static final String LOGIN_PAGE_XPATH = "//form[@name='loginForm']";

    private static final String LOGIN_FIELD_XPATH = "//input[@id='login-email']";
    private static final String PASSWORD_FIELD_XPATH = "//input[@id='login-password']";
    private static final String SUBMIT_BUTTON_XPATH = "//button[@type='submit']";
    private static final String INCORRECT_ALERT_XPATH = "//div[contains(@class, 'alert-danger')]";

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = LOGIN_PAGE_XPATH)
    private WebElement loginPage;

    @FindBy (xpath = LOGIN_FIELD_XPATH)
    private WebElement loginField;

    @FindBy (xpath = PASSWORD_FIELD_XPATH)
    private WebElement passwordField;

    @FindBy (xpath = SUBMIT_BUTTON_XPATH)
    private WebElement submitButton;

    @FindBy (xpath = INCORRECT_ALERT_XPATH)
    private WebElement incorrectAlert;

    public LoginPage openLoginPage() {
        getDriver().get(LOGIN_URL);
        waitElement();
        return this;
    }

    public boolean isDisplayedLoginPage(){
        return loginPage.isDisplayed();
    }

    public boolean isCorrectLoginPageUrl(){
        return getDriver().getCurrentUrl().equals("https://my-sandbox.maxpay.com/#/signin");
    }

    public boolean isDisplayedLoginField(){
        return loginField.isDisplayed();
    }

    public boolean isDisplayedPasswordField(){
        return passwordField.isDisplayed();
    }

    public boolean isEnabledSubmitButton(){
        return submitButton.isEnabled();
    }

    public void typeUserLogin(String login){
        loginField.clear();
        loginField.sendKeys(login);
    }

    public void typeUserPassword(String pass){
        passwordField.clear();
        passwordField.sendKeys(pass);
    }

    public HomePage clickSubmitButton(){
        submitButton.click();
        return new HomePage(getDriver());
    }

    public boolean isDisplayedIncorrectAlert(){
        return incorrectAlert.isDisplayed();
    }
}
