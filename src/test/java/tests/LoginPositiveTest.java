package tests;

import com.company.steps.HomeSteps;
import com.company.steps.LoginSteps;
import com.company.steps.PreconditionSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * @Author Anna Dvarri
 * Login Positive Tests<br>
 * These tests verify that login was success and 'Home' page is displayed
 * after enter correct data
 */

public class LoginPositiveTest extends BaseTestCase {

    private LoginSteps loginSteps;
    private HomeSteps homeSteps;

    /**
     * Precondition method make move to Login Page
     * */

    @BeforeClass
    public void openLoginPageAndVerifyThatLoginPageIsDisplayed(){
        PreconditionSteps preconditionSteps = new PreconditionSteps(getDriver());
        loginSteps = preconditionSteps.openLoginPage();
        loginSteps.verifyLoginUrlIsCorrect();
        loginSteps.verifyLoginPageIsDisplayed();

    }

    /*
    *  This test verifies that login was success and 'Home' page is displayed
    * */

    @Test
    public void verifyThatWhatLoginWasSuccessAndHomePageIsDisplayed() throws IOException {
        homeSteps = loginSteps.makeLoginViaUser(getUserLogin(), getUserPassword());
        homeSteps.verifyHomePageIsDisplayed();
        homeSteps.verifyHomeUrlIsCorrect();
    }



}
