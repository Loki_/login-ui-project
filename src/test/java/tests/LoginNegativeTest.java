package tests;

import com.company.steps.LoginSteps;
import com.company.steps.PreconditionSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * @Author Anna Dvarri
 * Login Negative Tests<br>
 * These tests verify that login was not success and 'Home' page is not displayed
 * but 'Incorrect alert' is displayed after enter incorrect data
 */
public class LoginNegativeTest extends BaseTestCase {


    private LoginSteps loginSteps;

    /**
     * Precondition method make move to Login Page
     * */

    @BeforeClass
    public void openLoginPageAndVerifyThatLoginPageIsDisplayed(){
        PreconditionSteps preconditionSteps = new PreconditionSteps(getDriver());
        loginSteps = preconditionSteps.openLoginPage();
        loginSteps.verifyLoginUrlIsCorrect();
        loginSteps.verifyLoginPageIsDisplayed();

    }

    /*
    *  This test verifies that login with incorrect data was not success and 'Home' page is not displayed
    * */

    @Test
    public void verifyThatLoginWithIncorrectDataWasNotSuccess() throws IOException {
        loginSteps.makeLoginViaUser(getUserLogin() + "1", getUserPassword() + "1");
        loginSteps.verifyIncorrectAlertIsDisplayed();
        loginSteps.verifyLoginUrlIsCorrect();
    }
}
